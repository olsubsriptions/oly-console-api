const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const indexService = require('./routes/index')
const userService = require('./routes/user.service.js')
const subscriptionService = require('./routes/subscription.service.js')
const applicationService = require('./routes/application.service.js')
const applicationBySubscriptionService = require('./routes/applicationBySubscription.service.js')

const app = express()
const corsOptions = {
  origin: process.env.CORS_ORIGIN,
  optionsSuccessStatus: 200
}
const cors2 = process.env.CORS_ORIGIN;
app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexService)
app.use('/users', userService)
app.use('/subscription', subscriptionService)
app.use('/application', applicationService)
app.use('/application/subscription', applicationBySubscriptionService)
module.exports = app
