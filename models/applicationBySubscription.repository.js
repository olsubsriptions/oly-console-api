const db = require('../config/firebase').db
const cryptoRepository = require('./crypto.repository.js')
const uuid = require('uuid/v4');
const Crypto = require('../class/crypto.js')
const constants = require('../constants/constants');
const ApplicationBySubscription = require('../class/applicationBySubscription.js')

async function createApplicationSuscription(data) {
    const applicationBySubscriptionId = uuid();
    let applicationBySubscription = new ApplicationBySubscription();
    applicationBySubscription.b4321 = data.b4321;
    applicationBySubscription.k0871 = data.k0871;

    const applicationBySubscriptionDoc = await db.collection(constants.T_APPLICATION_BY_SUBSCRIPTION)
        .where('b4321', '==', applicationBySubscription.b4321)
        .where('k0871', '==', applicationBySubscription.k0871).get();

    if (applicationBySubscriptionDoc.empty) {

        const applicationDoc = await db.collection(constants.T_APPLICATIONS)
            .doc(applicationBySubscription.k0871).get();

        applicationBySubscription.u4321 = applicationDoc.data().u4321;
        applicationBySubscription.w0792 = applicationDoc.data().k0874;
        applicationBySubscription.w0793 = applicationDoc.data().k0875;
        applicationBySubscription.w0794 = applicationDoc.data().k0876;
        applicationBySubscription.w0795 = applicationDoc.data().k0877;
        applicationBySubscription.w0796 = await cryptoRepository.getKey();
        applicationBySubscription.w0797 = "appData"
        applicationBySubscription.w0798 = applicationDoc.data().k0878;

        const applicationBySubscriptionRef = db.collection(constants.T_APPLICATION_BY_SUBSCRIPTION)
            .doc(applicationBySubscriptionId);

        const applicationBySubscriptionCreate = await applicationBySubscriptionRef.create({
            b4321: applicationBySubscription.b4321,
            k0871: applicationBySubscription.k0871,
            u4321: applicationBySubscription.u4321,
            w0792: applicationBySubscription.w0792,
            w0793: applicationBySubscription.w0793,
            w0794: applicationBySubscription.w0794,
            w0795: applicationBySubscription.w0795,
            w0796: applicationBySubscription.w0796,
            w0797: applicationBySubscription.w0797,
            w0798: applicationBySubscription.w0798
        })

        console.log(`w0790 updated at: ${applicationBySubscriptionCreate.writeTime.toDate()}`);
        return applicationBySubscription;
    } else {
        return null;
    }
}

async function getApplicationsBySubscription(subscriptionId) {

    let applicationsBySubscription = [];
    const applicationBySubscriptionRef = db.collection(constants.T_APPLICATION_BY_SUBSCRIPTION)
        .where('b4321', '==', subscriptionId);

    const applicationBySubscriptionDocs = await applicationBySubscriptionRef.get();

    for (const applicationBySubscriptionDoc of applicationBySubscriptionDocs.docs) {
        let applicationBySubscription = new ApplicationBySubscription();
        const applicationDoc = await db.collection(constants.T_APPLICATIONS)
            .doc(applicationBySubscriptionDoc.data().k0871).get();
        applicationBySubscription = applicationBySubscriptionDoc.data();
        applicationBySubscription.w0791 = applicationBySubscriptionDoc.id;
        applicationBySubscription.k0872 = applicationDoc.data().k0872;
        applicationBySubscription.k0880 = applicationDoc.data().k0880;
        applicationsBySubscription.push(applicationBySubscription);
    };
    return applicationsBySubscription;
}

async function getK2(userId, applicationId) {
    const userDocument = await db.collection(constants.T_USERS)
        .doc(userId).get();
    const applicationBySubscriptionRef = db.collection(constants.T_APPLICATION_BY_SUBSCRIPTION)
        .where('b4321', '==', userDocument.data().b4321).where('k0871', '==', applicationId);

    applicationBySubscriptionDocument = await applicationBySubscriptionRef.get();

    let crypto = new Crypto();
    crypto.keyType = constants.KEY_K2;
    crypto.objectId = applicationBySubscriptionDocument.docs[0].id;
    crypto.cipher = {
        algorithm: constants.ALGORITHM_AES256,
        charset: constants.CHARSET_UTF8
    };

    const k2 = await cryptoRepository.generateConfigurationData(crypto);
    return k2;
}

async function updateApplicationDerivativeKey(userId, applicationId) {
    const derivativeKey = await cryptoRepository.getKey();
    const userDocument = await db.collection(constants.T_USERS)
        .doc(userId).get();
    const applicationBySubscriptionQuery = db.collection(constants.T_APPLICATION_BY_SUBSCRIPTION)
        .where('b4321', '==', userDocument.data().b4321).where('k0871', '==', applicationId);

    const applicationBySubscriptionDocument = await applicationBySubscriptionQuery.get();
    const applicationBySubscriptionId = applicationBySubscriptionDocument.docs[0].id;

    const applicationBySubscriptionRef = db.collection(constants.T_APPLICATION_BY_SUBSCRIPTION)
        .doc(applicationBySubscriptionId);
    const applicationBySubscriptionUpdate = await applicationBySubscriptionRef.update(
        {
            w0796: derivativeKey
        }
    );

    console.log(`w0790 updated at: ${applicationBySubscriptionUpdate.writeTime.toDate()}`)
}

async function verifySubscription(requestBody) {

    const indexKey = requestBody.index_key;
    let data = requestBody.data;

    const subscriptionDoc = await db.collection(constants.T_SUBSCRIPTIONS)
        .where('b4325', '==', indexKey).get();

    if (!subscriptionDoc.empty) {
        const configDocument = await db.collection(constants.T_CONFIG)
            .doc('cnfg01').get();
        data = await cryptoRepository.decrypt(data, configDocument.data().tpksys);
        if (data) {
            console.log(JSON.parse(data));
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

async function verifySubscriptionByIndexKey(indexKey) {

    const subscriptionDoc = await db.collection(constants.T_SUBSCRIPTIONS)
        .where('b4325', '==', indexKey).get();

    if (!subscriptionDoc.empty) {
        const applicationBySubscriptionQuery = db.collection(constants.T_APPLICATION_BY_SUBSCRIPTION)
            .where('b4321', '==', subscriptionDoc.docs[0].id);
        const applicationBySubscriptionDocument = await applicationBySubscriptionQuery.get();

        if(!applicationBySubscriptionDocument.empty){
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

module.exports = {
    createApplicationSuscription,
    getApplicationsBySubscription,
    getK2,
    updateApplicationDerivativeKey,
    verifySubscription,
    verifySubscriptionByIndexKey
}