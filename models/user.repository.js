const db = require('../config/firebase').db
const cryptoRepository = require('../models/crypto.repository.js')
const User = require('../class/user.js')
const constants = require('../constants/constants');

async function getUserById(uid) {
    const userDocument = await db.collection(constants.T_USERS)
        .doc(uid).get();
    const suscriptionDocument = await db.collection(constants.T_ORGANIZATIONS)
        .doc(userDocument.data().b4321).get();

    const securityKey = suscriptionDocument.data().b4322;
    console.log(userDocument.data());
    var user = new User();
    user.g6731 = userDocument.id;
    user.g6732 = await cryptoRepository.decrypt(userDocument.data().g6732, securityKey);
    user.g6733 = userDocument.data().g6733;
    user.b4321 = userDocument.data().b4321;

    return user;
}

module.exports = {    
    getUserById
}