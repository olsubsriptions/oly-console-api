const axios = require('axios')
const sha384 = require('sha384')
// const cryptoURI = 'http://172.16.1.40:8085/'
const cryptoURI = 'https://cryptosvc-dot-subs-001.appspot.com/'
// const cryptoURI = process.env.CRYPTO_URI

async function getKey () {
  const key = await axios.post(cryptoURI + '/v1/crypto/keyGenerator', {
    keyType: 'SK',
    cipher: {
      algorithm: 'AES256',
      charset: 'UTF-8'
    }
  })
  return key.data.encKey
}

function hash (text) {
  const buffer = sha384(text)
  return buffer.toString('hex')
}

async function encrypt (text, key) {
  if (!text || !key) {
    return null
  }
  const response = await axios.post(cryptoURI + '/v1/crypto/encrypt', {
    dataList: [text],
    securityKey: key,
    cipher: {
      algorithm: 'AES256',
      charset: 'UTF-8'
    }
  })
  let cipher = null
  if (response && response.data && response.data.dataList && Array.isArray(response.data.dataList) && response.data.dataList.length > 0) {
    cipher = response.data.dataList[0]
  }
  return cipher
}

async function decrypt (text, key) {
  if (!text || !key) {
    return null
  }
  const response = await axios.post(cryptoURI + '/v1/crypto/decrypt', {
    dataList: [text],
    securityKey: key,
    cipher: {
      algorithm: 'AES256',
      charset: 'UTF-8'
    }
  })
  let cipher = null
  if (response && response.data && response.data.dataList && Array.isArray(response.data.dataList) && response.data.dataList.length > 0) {
    cipher = response.data.dataList[0]
  }
  return cipher;
}

async function generateConfigurationData (data) {
  if (!data) {
    return null;
  }
  const response = await axios.post(cryptoURI + '/v1/crypto/kDataGenerator', data);
  let k = null;
  if (response && response.data) {
    k = response.data.encKCnfgData;
  }
  return k;
}

module.exports = {
  getKey,
  encrypt,
  decrypt,
  generateConfigurationData,
  hash
}
