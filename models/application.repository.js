const db = require('../config/firebase').db;
const constants = require('../constants/constants');
const Application = require('../class/application.js');
const Component = require('../class/component.js');
const uuid = require('uuid/v4');

async function createApplication(application) {
    let newApplication = new Application();
    newApplication = application;
    newApplication.k0871 = uuid();

    const applicationRef = db.collection(constants.T_APPLICATIONS).doc(newApplication.k0871);
    const applicationCreate = await applicationRef.create({
        k0872: newApplication.k0872,
        k0873: newApplication.k0873,
        k0874: newApplication.k0874,
        k0875: newApplication.k0875,
        k0876: newApplication.k0876,
        k0877: newApplication.k0877,
        k0878: newApplication.k0878,
        k0879: newApplication.k0879,
        k0880: newApplication.k0880
    })

    if (applicationCreate) {
        console.log(`k0870 written at: ${applicationCreate.writeTime.toDate()}`);
        return newApplication;
    } else {
        return null;
    }   
}

async function createComponent(component) {
    let newComponent = new Component();
    newComponent = component;
    newComponent.c7051 = uuid();

    const componentRef = db.collection(constants.T_COMPONENTS).doc(newComponent.c7051);
    const componentCreate = await componentRef.create({
        k0871: newComponent.k0871,
        c7052: newComponent.c7052,
        c7053: newComponent.c7053
    })

    if (componentCreate) {
        console.log(`c7050 written at: ${componentCreate.writeTime.toDate()}`);
        return newComponent;
    } else {
        return null;
    }
}

async function getApplications() {
    var applications = [];
    const applicationCollection = await db.collection(constants.T_APPLICATIONS).get();
    applicationCollection.forEach(application => {
        let newApplication = new Application();
        newApplication.k0871 = application.id;
        newApplication.k0872 = application.data().k0872;
        newApplication.k0873 = application.data().k0873;
        newApplication.k0874 = application.data().k0874;
        newApplication.k0875 = application.data().k0875;
        newApplication.k0876 = application.data().k0876;
        newApplication.k0877 = application.data().k0877;
        newApplication.k0878 = application.data().k0878;
        newApplication.k0879 = application.data().k0879;
        newApplication.k0880 = application.data().k0880;
        applications.push(newApplication);
    });
    return applications;
}

module.exports = {
    createApplication,
    createComponent,
    getApplications
}