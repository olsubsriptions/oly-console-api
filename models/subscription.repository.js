const db = require('../config/firebase').db
const cryptoRepository = require('./crypto.repository.js')
const Crypto = require('../class/crypto.js')
const Subscription = require('../class/subscription.js')
const constants = require('../constants/constants');

async function getSubscriptionByUserId(userId) {
    const userDocument = await db.collection(constants.T_USERS)
        .doc(userId).get();
    const subscriptionDocument = await db.collection(constants.T_SUBSCRIPTIONS)
        .doc(userDocument.data().b4321).get();
    let subscription = new Subscription();
    subscription = subscriptionDocument.data();    
    subscription.b4321 = subscriptionDocument.id;
    
    return subscription;
}

async function getK1(userId) {
    const userDocument = await db.collection(constants.T_USERS)
        .doc(userId).get();
    const subscriptionDocument = await db.collection(constants.T_SUBSCRIPTIONS)
        .doc(userDocument.data().b4321).get();
    let crypto = new Crypto();
    crypto.keyType = constants.KEY_K1;
    crypto.objectId = subscriptionDocument.id;
    crypto.derivativeKey = subscriptionDocument.data().b4324;
    crypto.cipher = {
        algorithm: constants.ALGORITHM_AES256,
        charset: constants.CHARSET_UTF8
    };

    const k1 = await cryptoRepository.generateConfigurationData(crypto);
    return k1;
}

async function updateSubscriptionDerivativeKey(userId) {
    const derivativeKey = await cryptoRepository.getKey();
    const userDocument = await db.collection(constants.T_USERS)
        .doc(userId).get();
    const subscriptionRef = db.collection(constants.T_SUBSCRIPTIONS)
        .doc(userDocument.data().b4321);
    const subscriptionUpdate = await subscriptionRef.update(
        {
            b4324: derivativeKey
        }
    );
    console.log(`b4320 updated at: ${subscriptionUpdate.writeTime.toDate()}`)
}

module.exports = {
    getK1,
    updateSubscriptionDerivativeKey,
    getSubscriptionByUserId
}