const express = require('express')
const router = express.Router()
const Crypto = require('../class/crypto.js')
const Subscription = require('../class/subscription.js')
const subscriptionRepository = require('../models/subscription.repository.js')

router.get('', async function (req, res, next) {
    try {
        let subscription = new Subscription();
        subscription = await subscriptionRepository.getSubscriptionByUserId(req.query.userId);
        res.send(subscription);
    } 
    catch (error) {
        res.status(500).send({
            status: false,
            message: error.message
        })
    }
});

router.get('/k1', async function (req, res, next) {
    try {
        let crypto = new Crypto();
        const k1 = await subscriptionRepository.getK1(req.query.userId);
        crypto.encKCnfgData = k1;
        res.send(crypto);
    } 
    catch (error) {
        res.status(500).send({
            status: false,
            message: error.message
        })
    }
});

router.get('/regeneratek1', async function (req, res, next) {
    try {
        await subscriptionRepository.updateSubscriptionDerivativeKey(req.query.userId);
        let crypto = new Crypto();
        const k1 = await subscriptionRepository.getK1(req.query.userId);
        crypto.encKCnfgData = k1;
        res.send(crypto);
    }
    catch (error) {
        res.status(500).send({
            status: false,
            message: error.message
        })
    }
})

module.exports = router