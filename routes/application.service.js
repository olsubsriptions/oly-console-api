const express = require('express')
const router = express.Router()
const Application = require('../class/application.js')
const Component = require('../class/component.js')
const uuid = require('uuid/v4');
const { createApplication, createComponent, getApplications } = require('../models/application.repository.js')

router.post('', async function (req, res, next) {
    application = new Application();
    application = await createApplication(req.body);
    res.send(application);
});

router.get('', async function (req, res, next) {
    applications = await getApplications();
    res.send(applications);
});

router.post('/component', async function (req, res, next) {
    component = new Component();
    //component = await createComponent(req.body);
    res.send(component);
});

module.exports = router