const express = require('express')
const router = express.Router()
const Crypto = require('../class/crypto.js')
const ApplicationBySubscription = require('../class/applicationBySubscription.js')
const applicationBySubscriptionRepository = require('../models/applicationBySubscription.repository.js')

router.post('', async function (req, res, next) {
    try {
        let applicationBySubscription = new ApplicationBySubscription();
        applicationBySubscription = await applicationBySubscriptionRepository.createApplicationSuscription(req.body);
        if (applicationBySubscription) {
            res.send(applicationBySubscription);
        } else {
            res.send({
                status: false,
                message: 'Ya existe una suscripción asociada a esta aplicación'
            });
        }
    }
    catch (error) {
        res.status(500).send({
            status: false,
            message: error.message
        })
    }
});

router.get('', async function (req, res, next) {
    try {
        let applicationsBySubscription = await applicationBySubscriptionRepository.getApplicationsBySubscription(req.query.subscriptionId);
        res.send(applicationsBySubscription);
    }
    catch (error) {
        res.status(500).send({
            status: false,
            message: error.message
        })
    }
});

router.get('/k2', async function (req, res, next) {
    try {
        let crypto = new Crypto();
        const k2 = await applicationBySubscriptionRepository.getK2(req.query.userId, req.query.applicationId);
        crypto.encKCnfgData = k2;
        res.send(crypto);
    }
    catch (error) {
        res.status(500).send({
            status: false,
            message: error.message
        })
    }
});

router.get('/regeneratek2', async function (req, res, next) {
    try {
        await applicationBySubscriptionRepository.updateApplicationDerivativeKey(req.query.userId, req.query.applicationId);
        let crypto = new Crypto();
        const k2 = await applicationBySubscriptionRepository.getK2(req.query.userId, req.query.applicationId);
        crypto.encKCnfgData = k2;
        res.send(crypto);
    }
    catch (error) {
        res.status(500).send({
            status: false,
            message: error.message
        })
    }
})

router.post('/bind', async function (req, res, next) {
    try {
        let response = await applicationBySubscriptionRepository.verifySubscription(req.body);
        res.send({
            status: response,
            message: response ? 'valid': 'invalid'            
        })
    }
    catch (error) {
        res.status(500).send({
            status: false,
            message: error.message
        })
    }
})

router.get('/verify', async function(req, res, next){
    try {
        let response = await applicationBySubscriptionRepository.verifySubscriptionByIndexKey(req.query.indexKey);
        res.send({
            status: response,
            message: response ? 'valid': 'invalid'            
        })
    }
    catch (error){
        res.status(500).send({
            status: false,
            message: error.message
        })
    }
})

module.exports = router