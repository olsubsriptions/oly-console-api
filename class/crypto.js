function Crypto() {
    this.keyType = null,
    this.objectId = null,
    this.derivativeKey = null,
    this.encKey = null,
    this.encKCnfgData = null,
    this.dataList = [],
    this.cipher = {
        algorithm: null,
        charset: null
    }
}

module.exports = Crypto;