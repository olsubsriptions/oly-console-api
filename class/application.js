function Application() {
    this.k0871 = null, // ouid
    this.u4321 = null, // profileID
    this.k0872 = null, // applicationName
    this.k0873 = null, // applicationShortName
    this.k0874 = null, // version
    this.k0875 = null, // versionMajor
    this.k0876 = null, // versionMinor
    this.k0877 = null, // applicationEndpoint
    this.k0878 = null, // port
    this.k0879 = null, // state
    this.k0880 = null // image
}

module.exports = Application;