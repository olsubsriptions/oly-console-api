function ApplicationBySubscription () {
    this.w0791 = null, // applicationBySubscriptionId
    this.b4321 = null, // subscriptionId
    this.k0871 = null, // applicationId
    this.u4321 = null, // appliedProfileId
    this.w0792 = null, // version
    this.w0793 = null, // versionMajor
    this.w0794 = null, // versionMinor
    this.w0795 = null, // assignedApplicationEndpoint
    this.w0796 = null, // securityKey
    this.w0797 = null, // applicationData
    this.w0798 = null // applicationPort
}

module.exports = ApplicationBySubscription;